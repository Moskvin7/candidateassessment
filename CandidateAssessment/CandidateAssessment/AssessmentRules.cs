﻿using CandidateAssessment.Randomizer;

namespace CandidateAssessment;

public class AssessmentRules : AssessmentRulesBase
{
    public AssessmentRules() : this(4, 6, 2)
    {
    }

    private AssessmentRules(int totalPretestQuestionsCount,
        int totalOperationalQuestionsCount,
        int shouldStartWithPretestQuestionsCount)
    {
        TotalQuestionsCount = totalPretestQuestionsCount + totalOperationalQuestionsCount;
        TotalPretestQuestionsCount = totalPretestQuestionsCount;
        TotalOperationalQuestionsCount = totalOperationalQuestionsCount;
        ShouldStartWithPretestQuestionsCount = shouldStartWithPretestQuestionsCount;
    }
}