namespace CandidateAssessment.Randomizer.Tests;

#pragma warning disable CS8618
public class RandomizerTests
{
    private string _testletId;
    private List<Item> _correctItems;
    private List<Item> _incorrectPretestItems;
    private List<Item> _incorrectOperationalItems;
    private AssessmentRules _correctAssemblyRules;

    [SetUp]
    public void Setup()
    {
        _correctAssemblyRules = new AssessmentRules();
        _testletId = Guid.NewGuid().ToString();

        _correctItems = Enumerable.Range(0, _correctAssemblyRules.TotalOperationalQuestionsCount)
            .Select(i => new Item()
            {
                ItemId = i.ToString(),
                ItemType = EItemType.Operational
            })
            .Union(
                Enumerable.Range(_correctAssemblyRules.TotalOperationalQuestionsCount,
                        _correctAssemblyRules.TotalPretestQuestionsCount)
                    .Select(i => new Item()
                    {
                        ItemId = i.ToString(),
                        ItemType = EItemType.Pretest
                    })
            )
            .ToList();

        _incorrectPretestItems = Enumerable.Range(0, _correctAssemblyRules.TotalQuestionsCount + 1)
            .Select(i => new Item()
            {
                ItemId = i.ToString(),
                ItemType = EItemType.Pretest
            })
            .ToList();

        _incorrectOperationalItems = Enumerable.Range(0, _correctAssemblyRules.TotalQuestionsCount + 1)
            .Select(i => new Item()
            {
                ItemId = i.ToString(),
                ItemType = EItemType.Operational
            })
            .ToList();
    }

    [Test]
    public void Randomizer_ShouldRandomizeSuccessfully()
    {
        // Arrange 
        var testlet = new Testlet(_testletId, _correctItems, _correctAssemblyRules);

        // Act
        var randomizeResultItems = testlet.Randomize();

        // Assert
        var hasCorrectFirstPretestItems = randomizeResultItems
                                              .Take(_correctAssemblyRules.ShouldStartWithPretestQuestionsCount)
                                              .Count(x => x.ItemType == EItemType.Pretest) ==
                                          _correctAssemblyRules.ShouldStartWithPretestQuestionsCount;
        var hasCorrectPretestItemsCount = randomizeResultItems
            .Count(x => x.ItemType == EItemType.Pretest) == _correctAssemblyRules.TotalPretestQuestionsCount;
        var hasCorrectOperationalItemsCount = randomizeResultItems
            .Count(x => x.ItemType == EItemType.Operational) == _correctAssemblyRules.TotalOperationalQuestionsCount;

        Assert.That(hasCorrectFirstPretestItems && hasCorrectOperationalItemsCount && hasCorrectPretestItemsCount,
            Is.True);
    }

    [Test]
    public void Randomizer_ShouldBeWithCorrectCountOfBeginningPretest()
    {
        // Arrange 
        var testlet = new Testlet(_testletId, _correctItems, _correctAssemblyRules);

        // Act
        var randomizeResultItems = testlet.Randomize();

        // Assert
        Assert.That(randomizeResultItems
                .Take(2)
                .Count(x => x.ItemType == EItemType.Pretest),
            Is.EqualTo(_correctAssemblyRules.ShouldStartWithPretestQuestionsCount));
    }

    [Test]
    public void Randomizer_WithIncorrectNumberOfElements_ShouldThrowArgumentException()
    {
        Assert.Throws<ArgumentException>(() =>
        {
            var _ = new Testlet(_testletId, _incorrectOperationalItems, _correctAssemblyRules);
        });
    }

    [Test]
    public void Randomizer_WithIncorrectNumberOfOperationalElements_ShouldThrowArgumentException()
    {
        Assert.Throws<ArgumentException>(() =>
        {
            var _ = new Testlet(_testletId, _incorrectOperationalItems, _correctAssemblyRules);
        });
    }

    [Test]
    public void Randomizer_WithIncorrectNumberOfPretestElements_ShouldThrowArgumentException()
    {
        Assert.Throws<ArgumentException>(() =>
        {
            var _ = new Testlet(_testletId, _incorrectPretestItems, _correctAssemblyRules);
        });
    }

    [Test]
    public void Randomizer_WithNullTestlet_ShouldThrowArgumentNullException()
    {
        Assert.Throws<ArgumentNullException>(() =>
        {
            var _ = new Testlet(null!, _incorrectPretestItems, _correctAssemblyRules);
        });
    }

    [Test]
    public void Randomizer_ShouldHasCorrectNumberOfPretestElementsInLastPart()
    {
        // Arrange 
        var testlet = new Testlet(_testletId, _correctItems, _correctAssemblyRules);

        // Act
        var randomizeResultItems = testlet.Randomize();

        // Assert
        Assert.That(randomizeResultItems
                .Skip(_correctAssemblyRules.ShouldStartWithPretestQuestionsCount)
                .Count(x => x.ItemType == EItemType.Pretest),
            Is.EqualTo(
                _correctAssemblyRules.TotalPretestQuestionsCount -
                _correctAssemblyRules.ShouldStartWithPretestQuestionsCount));
    }

    [Test]
    public void Randomizer_ShouldGenerateRandomlyAlways()
    {
        // Arrange 
        var testlet = new Testlet(_testletId, new List<Item>(_correctItems), _correctAssemblyRules);
        var testlet1 = new Testlet(_testletId, new List<Item>(_correctItems), _correctAssemblyRules);
        var testlet2 = new Testlet(_testletId, new List<Item>(_correctItems), _correctAssemblyRules);
        var testlet3 = new Testlet(_testletId, new List<Item>(_correctItems), _correctAssemblyRules);

        // Act
        var randomizeResultItems = testlet.Randomize();
        var randomizeResultItems1 = testlet1.Randomize();
        var randomizeResultItems2 = testlet2.Randomize();
        var randomizeResultItems3 = testlet3.Randomize();

        var hasUniqueRandomization = false;

        for (var i = 0; i < randomizeResultItems.Count; i++)
        {
            if (randomizeResultItems[i].ItemId != randomizeResultItems1[i].ItemId
                || randomizeResultItems[i].ItemId != randomizeResultItems2[i].ItemId
                || randomizeResultItems[i].ItemId != randomizeResultItems3[i].ItemId)
            {
                hasUniqueRandomization = true;
            }
        }

        // Assert
        Assert.That(hasUniqueRandomization, Is.True);
    }
}