﻿namespace CandidateAssessment.Randomizer;

public class Item
{
    public string ItemId { get; init; }
    public EItemType ItemType { get; init; }
}