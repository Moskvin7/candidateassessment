﻿namespace CandidateAssessment.Randomizer;

public class Testlet
{
    private readonly string _testletId;
    private readonly List<Item> _items;
    private readonly AssessmentRulesBase _assessmentRules;
    private static readonly Random Randomizer = new(0);

    public Testlet(string testletId, List<Item> items, AssessmentRulesBase assessmentRules)
    {
        if (string.IsNullOrWhiteSpace(testletId))
        {
            throw new ArgumentNullException(nameof(testletId));
        }

        if (items.Count != assessmentRules.TotalQuestionsCount 
            || items.Count(x=>x.ItemType == EItemType.Operational) != assessmentRules.TotalOperationalQuestionsCount
            || items.Count(x=>x.ItemType == EItemType.Pretest) != assessmentRules.TotalPretestQuestionsCount)
        {
            throw new ArgumentException(null, nameof(items));
        }
        
        _testletId = testletId;
        _items = items;
        _assessmentRules = assessmentRules;
    }

    public List<Item> Randomize()
    {
        _items.Sort(CompareItemByItemType);
        
        //Items private collection has 6 Operational and 4 Pretest Items. Randomize the order of these items as per the requirement (with TDD)
        //The assignment will be reviewed on the basis of – Tests written first, Correct logic, Well structured & clean readable code.
        for(var i = _assessmentRules.ShouldStartWithPretestQuestionsCount; i < _items.Count - 1; i++)
        {
            var randomUniqueNumber = GenerateUniqueNumber(i, _items.Count);
            
            (_items[i], _items[randomUniqueNumber]) = (_items[randomUniqueNumber], _items[i]);
        }

        return _items;
    }

    private static int GenerateUniqueNumber(int from, int to) => Randomizer.Next(from, to);
    
    private static int CompareItemByItemType(Item first, Item second)
    {
        if (first.ItemType == second.ItemType) return 0;

        if (first.ItemType == EItemType.Operational) return 1;

        return -1;
    }
}