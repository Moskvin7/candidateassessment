﻿namespace CandidateAssessment.Randomizer;

public abstract class AssessmentRulesBase
{
    public int TotalQuestionsCount { get; protected init; }
    public int TotalPretestQuestionsCount { get; protected init; }
    public int TotalOperationalQuestionsCount { get; protected init; }
    public int ShouldStartWithPretestQuestionsCount { get; protected init; }
}