﻿namespace CandidateAssessment.Randomizer;

public enum EItemType
{
    Pretest = 0,
    Operational = 1
}